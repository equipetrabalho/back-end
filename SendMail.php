<?php
/*********************************************************/
/*           INFORMAÇÕES DO FORMULÁRIO                   */
/*********************************************************/
	$nome 		= addslashes($_POST['nome']);
	$email 		= addslashes($_POST['email']);
	$mensagem 	= addslashes($_POST['mensagem']);

/*********************************************************/
/*           INFORMAÇÕES GERAIS DO E-MAIL                */
/*********************************************************/
	$destinatário = "usuario@teste.com.br"; # E-mail do destinatário
	$assunto = "Assunto do E-mail"; # Informação sobre o Assunto do E-mail
	$estrutura_email = "Nome do Remetente: ".$nome."\r\n".
					   "E-mail: ".$email."\r\n".
					   "Mensagem: ".$mensagem;

/*********************************************************/
/*           INFORMAÇÕES HEADER DO E-MAIL                */
/*********************************************************/
/*  From: E-mail responsável pela envio da mensagem      */
/*********************************************************/
/*  Reply-To: E-mail de resposta                         */
/*********************************************************/
/*  X-Mailer: PHP/versão do php                          */
/*   --> Reduz a chance do e-mail ir parar no            */
/*          Lixo Eletrônico ou Spam                      */
/*********************************************************/
	$header_mail = "From: contato@teste.com.br"."\r\n".
			  	   "Reply-To: ".$email."\r\n".
			  	   "X-Mailer: PHP/".phpversion();

# Função mail() para realizar o envio do e-mail
mail($destinatário, $assunto, $estrutura_email, $header_mail);

echo "<h2>Mensagem enviada com Sucesso!</h2>";
exit;

?>
